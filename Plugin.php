<?php namespace Keios\StudioManager;

use Backend;
use System\Classes\PluginBase;
use RainLab\Blog\Controllers\Posts as PostsController;
use RainLab\Blog\Models\Post as PostModel;

/**
 * StudioManager Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'StudioManager',
            'description' => 'Główna wtyczka dla modułów strony StudioManager',
            'author'      => 'Keios Solutions',
            'icon'        => 'icon-leaf',
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        PostsController::extendFormFields(
            function ($form, $model) {
                if (!$model instanceof PostModel) {
                    return;
                }
                $form->addSecondaryTabFields(
                    [
                        'yt_thumb'       => [
                            'label' => 'YouTube video code for thumbnail',
                            'tab'   => 'rainlab.blog::lang.post.tab_manage',
                        ],
                        'preview@update' => [
                            'type' => 'partial',
                            'path' => '$/keios/studiomanager/partials/_preview_link.htm',
                            'tab'  => 'rainlab.blog::lang.post.tab_manage',
                        ],
                    ]
                );
            }
        );
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Keios\StudioManager\Components\Calendar'     => 'studiomanager_calendar',
            'Keios\StudioManager\Components\ClosestEvent' => 'studiomanager_closest_event',
            'Keios\StudioManager\Components\CloseEvents'  => 'studiomanager_close_events',
            'Keios\StudioManager\Components\PostPreview'  => 'blogPostPreview',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'keios.studiomanager.events'    => [
                'tab'   => 'StudioManager',
                'label' => 'Dostęp do tworzenia i edycji wydarzeń',
            ],
            'keios.studiomanager.studios'   => [
                'tab'   => 'StudioManager',
                'label' => 'Dostęp do tworzenia i edycji kategorii wydarzeń',
            ],
            'keios.studiomanager.needs'     => [
                'tab'   => 'StudioManager',
                'label' => 'Dostęp do tworzenia i edycji potrzeb StudioManager',
            ],
            'keios.studiomanager.addresses' => [
                'tab'   => 'StudioManager',
                'label' => 'Dostęp do tworzenia i edycji adresów',
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        $navMenu = [
            'studiomanager' => [
                'label'       => 'StudioManager',
                'url'         => Backend::url('keios/studiomanager/events'),
                'icon'        => 'icon-play-circle',
                'permissions' => ['keios.studiomanager.*'],
                'order'       => 500,
                'sideMenu'    => [
                    'events'            => [
                        'label'       => 'keios.studiomanager::lang.events.menu_label',
                        'icon'        => 'icon-calendar',
                        'url'         => Backend::url('keios/studiomanager/events'),
                        'permissions' => ['keios.studiomanager.events'],
                        'order'       => 400,
                    ],
                    'studios'           => [
                        'label'       => 'keios.studiomanager::lang.studios.menu_label',
                        'url'         => Backend::url('keios/studiomanager/studios'),
                        'icon'        => 'icon-folder',
                        'permissions' => ['keios.studiomanager.studios'],
                        'order'       => 500,
                    ],
                    'addressoverwrites' => [
                        'label'       => 'keios.studiomanager::lang.addressoverwrites.menu_label',
                        'icon'        => 'icon-envelope',
                        'url'         => Backend::url('keios/studiomanager/addressoverwrites'),
                        'permissions' => ['keios.studiomanager.addresses'],
                        'order'       => 600,
                    ],
                ],
            ],
        ];

        return $navMenu;
    }

}
