<?php namespace Keios\StudioManager\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Lang;

/**
 * Plugin Manual Back-end Controller
 */
class PluginManual extends Controller
{
    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Keios.StudioManager', 'studiomanager', 'pluginmanual');
    }

    public function index()
    {
        $this->pageTitle = 'Strona StudioManager - Pomoc';
    }

}
