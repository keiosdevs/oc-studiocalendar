<?php namespace Keios\StudioManager\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Keios\StudioManager\Classes\RRValidator;
use Keios\StudioManager\Models\Event;
use Keios\StudioManager\Models\Settings;
use Lang;

/**
 * Events Back-end Controller
 */
class Events extends Controller {

    /**
     * @var array
     */
    public $implement = [
		'Backend.Behaviors.FormController',
		'Backend.Behaviors.ListController',
	];

    /**
     * @var string
     */
    public $formConfig = 'config_form.yaml';
    /**
     * @var string
     */
    public $listConfig = 'config_list.yaml';

    /**
     * Events constructor.
     */
    public function __construct() {
		parent::__construct();

		BackendMenu::setContext('Keios.StudioManager', 'studiomanager', 'events');
	}

    /**
     * Deleted checked events.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {
            foreach ($checkedIds as $eventId) {
                if (!$event = Event::find($eventId)) {
                    continue;
                }
                $event->delete();
            }
            Flash::success(Lang::get('keios.studiomanager::lang.events.delete_selected_success'));
        } else {
            Flash::error(Lang::get('keios.studiomanager::lang.events.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}