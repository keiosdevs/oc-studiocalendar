<?php namespace Keios\StudioManager\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Keios\StudioManager\Models\Studio;
use Lang;

/**
 * Categories Back-end Controller
 */
class Studios extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Keios.StudioManager', 'studiomanager', 'studios');
    }

    /**
     * Deleted checked studios.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {
            foreach ($checkedIds as $categoryId) {
                if (!$category = Studio::find($categoryId)) {
                    continue;
                }
                $category->delete();
            }
            Flash::success(Lang::get('keios.studiomanager::lang.studios.delete_selected_success'));
        } else {
            Flash::error(Lang::get('keios.studiomanager::lang.studios.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}