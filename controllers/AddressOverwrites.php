<?php namespace Keios\StudioManager\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Lang;
use Keios\StudioManager\Models\AddressOverwrite;

/**
 * Address Overwrites Back-end Controller
 */
class AddressOverwrites extends Controller
{
    /**
     * @var array
     */
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
    ];

    /**
     * @var string
     */
    public $formConfig = 'config_form.yaml';
    /**
     * @var string
     */
    public $listConfig = 'config_list.yaml';

    /**
     * AddressOverwrites constructor.
     */
    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Keios.StudioManager', 'studiomanager', 'addressoverwrites');
    }

    /**
     * Deleted checked addressoverwrites.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {
            foreach ($checkedIds as $addressoverwriteId) {
                if (!$addressoverwrite = AddressOverwrite::find($addressoverwriteId)) {
                    continue;
                }
                $addressoverwrite->delete();
            }
            Flash::success(Lang::get('keios.studiomanager::lang.addressoverwrites.delete_selected_success'));
        } else {
            Flash::error(Lang::get('keios.studiomanager::lang.addressoverwrites.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}
