<?php namespace Keios\StudioManager\Models;

use Carbon\Carbon;
use Model;

/**
 * event Model
 */
class Event extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'keios_studiomanager_events';

    /**
     * Validation rules
     */
    public $rules = [
        'date' => 'date|required',
        'name' => 'required',
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = [];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        '*',
    ];
    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var bool
     */
    public $exists = false;

    /**
     * @var array
     */
    protected $dates = ['date'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'studio' => [
            'Keios\StudioManager\Models\Studio',
            'table'    => 'keios_studiomanager_studios_studios'
        ],
    ];

    public function beforeSave()
    {
        $now = Carbon::now();
        $start = $now->copy()->subYear()->startOfMonth();
        $end = $now->copy()->addYear()->endOfMonth();
        $startString = str_replace(' ', '', $start->toDateString());
        $endString = str_replace(' ', '', $end->toDateString());
        $cacheString = 'ev'.$startString.$endString;
        \Cache::forget($cacheString);


    }
}
