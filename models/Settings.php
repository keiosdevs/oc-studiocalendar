<?php namespace Keios\StudioManager\Models;

use Model;

/**
 * Settings Model
 */
class Settings extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'keios_studiomanager_settings';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}