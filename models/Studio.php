<?php namespace Keios\StudioManager\Models;

use Form;
use Model;
use System\Classes\PluginManager;

/**
 * Studio Model
 */
class Studio extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var array
     */
    public $permOptions = [];

    /**
     * @var string
     */
    public $table = 'keios_studiomanager_studios';


    /**
     * @var array
     */
    public $rules = [
        'name' => 'required',
        'slug' => 'required|between:3,64|unique:keios_studiomanager_studios',
    ];

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    public $hasMany = [
        'events' => ['Keios\StudioManager\Models\Event'],
    ];
}
