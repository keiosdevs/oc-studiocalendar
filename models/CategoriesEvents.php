<?php namespace Keios\StudioManager\Models;

use Model;

/**
 * StudiosEvents Model
 */
class StudiosEvents extends Model {

	/**
	 * @var string The database table used by the model.
	 */
	public $table = 'keios_studiomanager_studios_events';

	/**
	 * @var array Guarded fields
	 */
	protected $guarded = [];

	/**
	 * @var array Fillable fields
	 */
	protected $fillable = ['*'];

	/**
	 * @var array Relations
	 */
	public $hasOne = [
		'category' => ['Keios\StudioManager\Models\Studio',
			'table' => 'keios_studiomanager_category',
		],
		'event' => ['Keios\StudioManager\Models\Event',
			'table' => 'keios_studiomanager_event',
		],
	];

}