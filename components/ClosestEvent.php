<?php namespace Keios\StudioManager\Components;

use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use Keios\StudioManager\Models\AddressOverwrite;
use Keios\StudioManager\Models\Event;

/**
 * Class ClosestEvent
 * @package Keios\StudioManager\Components
 */
class ClosestEvent extends ComponentBase
{

    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'keios.studiomanager::lang.components.closestevent.name',
            'description' => 'keios.studiomanager::lang.components.closestevent.description',
        ];
    }

    /**
     * @return array
     */
    public function defineProperties()
    {
        return [];
    }

    /**
     *
     * @throws \ApplicationException
     * @throws \Facebook\Exceptions\FacebookSDKException
     * @throws \October\Rain\Exception\ApplicationException
     */
    public function onRun()
    {
        $startDate = Carbon::now();
        $endDate = $startDate->copy()->addYears(1);
        $events = Event::where('is_published', 1)->whereBetween('start_date', [$startDate, $endDate])->get();
        $closestEvent = $this->findClosestEvent($startDate, $events);
        $this->page['closest_event'] = $closestEvent;
    }

    /**
     * @param Carbon $now
     * @param array  $events
     *
     * @return Event
     */
    private function findClosestEvent($now, $events)
    {
        $selectedEvent = null;
        $diffArray = [];
        $eventsArray = [];
        foreach ($events as $event) {
            $diffArray[$event->id] = $now->diffInSeconds(new Carbon($event->start_date));
            $eventsArray[$event->id] = $event;
        }
        $lowestId = array_keys($diffArray, min($diffArray));
        if (count($lowestId) > 0) {
            $selectedEvent = $eventsArray[$lowestId[0]];
        }

        return $selectedEvent;
    }

    /**
     * @param string $alias
     *
     * @return string
     */
    public function getTrueLocation($alias)
    {
        $row = AddressOverwrite::where('title', $alias)->first();
        if ($row) {
            return $row->address;
        }

        return $alias;
    }
}