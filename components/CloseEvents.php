<?php namespace Keios\StudioManager\Components;

use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use Keios\StudioManager\Models\Event;

/**
 * Class CloseEvents
 * @package Keios\StudioManager\Components
 */
class CloseEvents extends ComponentBase
{

    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'keios.studiomanager::lang.components.closeevents.name',
            'description' => 'keios.studiomanager::lang.components.closeevents.description'
        ];
    }

    /**
     * @return array
     */
    public function defineProperties()
    {
        return [
            'amount' => [
                'title'       => 'keios.studiomanager::lang.close_events.amount',
                'description' => 'keios.studiomanager::lang.close_events.amount_desc',
                'default'     => '4',
                'type'        => 'number'
            ],
        ];
    }

    public function onRun()
    {
        $amount = $this->property('amount');
        $events = $this->findClosestEvents($amount);
        $this->page['close_events'] = $events;
    }

    private function findClosestEvents($amount)
    {
        $now = Carbon::now();
        $yearLater = $now->copy()->addYear();
        return Event::where('is_published', 1)
            ->whereBetween('start_date', [$now, $yearLater])
            ->orderBy('start_date', 'asc')
            ->take($amount)
            ->get();

    }
}