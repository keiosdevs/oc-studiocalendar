<?php namespace Keios\StudioManager\Components;

use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use Keios\StudioManager\Models\AddressOverwrite;
use Keios\StudioManager\Models\Event;

class Calendar extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Calendar Component',
            'description' => 'No description provided yet...',
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    /**
     *
     * @throws \October\Rain\Exception\ApplicationException
     */
    public function onRun()
    {
        $events = $this->getEvents();

        $eventsJson = $this->createFullCalendarEvents($events);
        $this->page['studiomanager_events_json'] = $eventsJson;
        $this->page['studiomanager_events_raw'] = $events;
    }


    private function getEvents()
    {
        $now = Carbon::now();
        $start = $now->copy()->subYear()->startOfMonth();
        $end = $now->copy()->addYear()->endOfMonth();
        $startString = str_replace(' ', '', $start->toDateString());
        $endString = str_replace(' ', '', $end->toDateString());
        $cacheString = 'ev'.$startString.$endString;
        $events = Event::where('is_taken', 1)->whereBetween('start_date', [$start, $end])->rememberForever(
            $cacheString
        )->get();

        return $events;
    }

    /**
     * @param array $events
     *
     * @return string
     */
    private function createFullCalendarEvents($events)
    {
        $fcArray = [];
        foreach ($events as $event) {
            $fcEvent = [
                'id'          => $event->id,
                'title'       => 'Zajęte',
                'description' => $event->location,
                'start'       => $event->start_date,
                'end'         => $event->end_date,
                'color'       => $event->color,
                'textColor'   => $event->font_color,
            ];
            $fcArray[] = $fcEvent;
        }

        return json_encode($fcArray);
    }

    /**
     * @param string $alias
     *
     * @return string
     */
    public function getTrueLocation($alias)
    {
        $row = AddressOverwrite::where('title', $alias)->first();
        if ($row) {
            return $row->address;
        }

        return $alias;
    }

}