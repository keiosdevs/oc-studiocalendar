<?php namespace Keios\StudioManager\Updates;

use Illuminate\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use Schema;

class CreateEventsTable extends Migration
{

    public function up()
    {
        Schema::create(
            'keios_studiomanager_events',
            function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->integer('studio_id')->unsigned();
                $table->string('client');
                $table->string('color')->nullable();
                $table->string('font_color')->nullable();
                $table->dateTime('start_date');
                $table->dateTime('end_date')->nullable();
                $table->text('description');
                $table->boolean('is_taken')->default(true);
                $table->timestamps();
            });
    }

    public function down()
    {
        Schema::dropIfExists('keios_studiomanager_events');
    }

}
