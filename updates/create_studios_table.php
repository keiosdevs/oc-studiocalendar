<?php namespace Keios\StudioManager\Updates;

use Illuminate\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use Schema;

/**
 * Class CreateCategoriesTable
 * @package Keios\StudioManager\Updates
 */
class CreateStudiosTable extends Migration
{

    /**
     *
     */
    public function up()
    {
        Schema::create('keios_studiomanager_studios', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->string('location');
            $table->string('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     *
     */
    public function down()
    {
        Schema::dropIfExists('keios_studiomanager_studios');
    }

}
