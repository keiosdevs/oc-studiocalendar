<?php namespace Keios\StudioManager\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateAddressOverwritesTable extends Migration
{
    public function up()
    {
        Schema::create(
            'keios_studiomanager_address_overwrites',
            function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('title');
                $table->string('address');
                $table->timestamps();
            });
    }

    public function down()
    {
        Schema::dropIfExists('keios_studiomanager_address_overwrites');
    }
}
