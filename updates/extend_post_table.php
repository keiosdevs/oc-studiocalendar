<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 11/24/16
 * Time: 11:10 PM
 */

namespace Keios\StudioManager\Updates;

use Illuminate\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;
use DB;


class ExtendPostTables extends Migration
{
    public function up()
    {
        Schema::table(
            'rainlab_blog_posts',
            function (Blueprint $table) {
                $table->string('yt_thumb')->after('content_html')->nullable();
            }
        );
    }

    public function down()
    {
        Schema::table(
            'rainlab_blog_posts',
            function (Blueprint $table) {
                $table->dropColumn('yt_thumb');
            }
        );
    }

}